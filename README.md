USAC
SA
Marellyn Lisbeth Trejo Castro - 201313688
Detalles del proyecto
El proyecto es una aplicación SOApara uber etas:
Solicitud de servicio por parte del cliente
Recepción de solicitud y aviso al restaurante
Pre-requisitos
Para la implementacion del proyecto se necesitan los siguientes requisitos
Sistema Operativo, necesitaremos Windows Profesional
Instalación de IIS
.NET framework 3.5 o superior
Proyecto alojado
Arquitectura
Para el proyecto se llevo la siguiente arquitectura de micro-servicios orquestado por ESB Arquitectura
Video
https://www.youtube.com/watch?v=zngKseXzJp0&feature=youtu.be
Definición de los servicios
Servicio Cliente
El servicio de cliente tiene como atributos su nombre de usuario y la zona actual en la que reside, esta zona es la que se obtiene y se utiliza para la solicitud de viajes, dando solicitudes al EBS para pedir un menu
Servicio Piloo
El servicio de pilotos contiene los atributos de código de piloto, el cual seria único y sobre el cual se pueden realizar actualizaciones, un estado que puede ser disponible u ocupado dependiendo si esta en un viaje y una ubicación que es en la que esta actualmente, esto para verificar que se este mandando el piloto mas cercano al realizar una petición de viaje.
Servicio Menu
El servicio de rastreo es el mas importante, este servicio le pide al ESB todos los pilotos disponibles y con esta información es el encargado de realizar todos los cálculos de que piloto es el mejor, cual seria el precio y retornar los datos de todo esto al ESB
Interacción servicios con ESB
Nuestro enterprise service bus es el orquestador de todos nuestros micro-servicios, por esto se encuentras multiples interacciones de los servicios con el ESB y el ESB con los servicios y ninguna interacción entre servicios, las principales son:
ESB
SolicitudViajeCliente
Función alojada en el ESB, lo que recibe es una solicitud de viaje con una zona de ubicación y este realiza una petición al servidor de rastreo por medio de ObtenerPropuestaPiloto, mandando de parámetro la misma zona recibida.
Solicitud Menu
Función alojada en el ESB, lo que recibe es una solicitud de un servidor para obtener todos los pilotos con estado disponible, por lo cual este realiza una petición al servicio de pilotos por medio de ObtenerConductoresDisponibles para que le devuelva todos los menus disponibles.
IngresoCliente
Función alojada en el ESB, recibe una solicitud para iniciar una sesión con un usuario, recibe de parámetro el nombre de usuario y la zona donde se ubica actualmente el usuario, el ESB realiza una petición al servidor de usuarios para la inserción por medio de IngresoCliente.
Ocupar Resturamete
unción alojada en el ESB, recibe una solicitud para marcar como ocupado un piloto y este manda la petición al servicio de pilotos por medio de OcuparPiloto.
Servidor Cliente
IngresoCliente
Función alojada en el servidor de cliente, lo que recibe es una petición de ingreso por medio del ESB y este actualiza el usuario con la sesión actual iniciada.
Servidor Piloto
ObtenerConductoresDisponibles
Función alojada en el servidor de pilotos, lo que recibe es una petición del ESB para obtener todos los conductores cuyo estado esta disponible,