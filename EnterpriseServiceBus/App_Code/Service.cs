﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

public class Service : System.Web.Services.WebService
{
    // Instancia de variables globales, guion bajo al principio como estandard de codigo

    // Instancia de servidor de Rest
    ServidorRest.Service_RestSoapClient _Rest = new ServidorRest.Service_RestSoapClient();
    // Instancia de servidor de pilotos
    ServidorPiloto.Service_PilotoSoapClient _pilotos = new ServidorPiloto.Service_PilotoSoapClient();
    // Instancia de servidor de clientes
    ServidorCliente.Service_ClienteSoapClient _clientes = new ServidorCliente.Service_ClienteSoapClient();

    /*
    * Metodo del servidor web que recibe la solicitud de viaje de un cliente
    * Parametros: *menu --> tipo entero que cotiene la menu actual del cliente 
    * Realiza una llamada al servidor de Rests para obtener la propuesta del piloto mas cercano
    */ 
    [WebMethod]
    public String SolicitudViajeCliente(int menu)
    {
        return _Rest.ObtenerPropuestaPiloto(menu);
    }

    /*
    * Metodo del servidor web que recibe la solicitud de pilotos disponibles 
    * Parametros: ninguno 
    * Realiza una llamada al servidor de pilotos para obtener la lista de conductores disponibles
    */ 
    [WebMethod]
    public string SolicitudPilotosDisponibles()
    {
        
        return "";
    }

    /*
    * Metodo del servidor web que recibe la solicitud de modificar el estado de un piloto a ocupado
    * Parametros: *codigoPiloto --> entero que contiene el codigo unico del piloto a cambiar el estado 
    * Realiza una llamada al servidor de pilotos para modificar el estado del piloto
    */ 
    [WebMethod]
    public void OcuparPiloto(int codigoPiloto) 
    {
        _pilotos.OcuparPiloto(codigoPiloto);
    }

    /*
    * Metodo del servidor web que recibe la solicitud el ingreso de un cliente
    * Parametros: *nombreUsuario --> cadena que contiene el nombre del usuario
    *             *menuActual    --> entero que contiene la menu actual del usuario que intenta registrarse 
    */
    [WebMethod]
    public Boolean IngresoCliente(String nombreUsuario, int menuActual)
    {
        return true;
    }
}