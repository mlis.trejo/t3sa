﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de DisponPiloto
/// </summary>
public class DisponPiloto
{
    // Declaracion de variables globales

    // Declaracion de lista con todos los pilotos disponibles
    private LinkedList<EntidadPiloto> _listaPilotos;
    // Declaracion de un random, por motivos del demo
    Random _rnd = new Random();


    /*
    * Constructor para la gestion de pilotos
    * Parametros: ninguno
    * nstancia la lista de pilotos y para motivos de la simulacion se insertan 8 pilotos
    */
    public DisponPiloto() {
        _listaPilotos = new LinkedList<EntidadPiloto>();
        AgregarPiloto(new EntidadPiloto("123ABC", EntidadPiloto.Tipo_Estado.DISPONIBLE, 1,"Marellyn Trejo"));
        AgregarPiloto(new EntidadPiloto("456DEF", EntidadPiloto.Tipo_Estado.DISPONIBLE, 2, "Marioly Trejo"));
        AgregarPiloto(new EntidadPiloto("789GHI", EntidadPiloto.Tipo_Estado.DISPONIBLE, 3, "William Trejo"));
    }

    /*
    * Metodo para agregar nuevos pilotos 
    * Parametros: *EntidadPiloto, nodo que contiene todos los datos del piloto
    * agrega el piloto mandado a la lista de pilotos
    */
    public void AgregarPiloto(EntidadPiloto piloto) {
        _listaPilotos.AddLast(piloto);
    }


    public String ObtenerInformacion(string placa) {

        foreach (EntidadPiloto piloto in _listaPilotos)
        {
            if (piloto.GetPlaca() == placa)
            {
                return "Nombre Piloto: " + piloto.GetNombre() + "\n" +
                    "Placa Carro: " + piloto.GetPlaca() + "\n" +
                    "Zona Actual: " + piloto.GetZonaCubierta() + "\n" +
                    "Tiempo Estimado: " + _rnd.Next(1, 15); 
            }
        }
        return "Sin pilotos disponibles";
    }

    /*
    * Metodo para el cambio de estado del piloto
    * Parametros: *codigo    --> entero que contiene el codigo del piloto que se desea modificar
    * Recorre todos los pilotos y cambia el necesario
    */
    public void OcuparPiloto(int codigo) 
    {
        // Ciclo iterador para recorrer todos los pilotos disponibles
        foreach (EntidadPiloto piloto in _listaPilotos)
        {
           
        }
    }
}